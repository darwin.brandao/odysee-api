#!/bin/bash

# Store the script path in a variable
DIR=$( cd -- "$( dirname "$(readlink -f "$0")" )" &> /dev/null && pwd )

function usage()
{
    echo "Usage: ${0} [OPTIONS] [-b VALUE]"
    echo "  -i      Channel Claim Id"
    echo "  -h      Display help"
    exit 1
}

# Input Handler
source "${DIR}/input-handler-/subscribers.src"

function get_by_channel(){

	json="$( curl -s 'https://api.odysee.com/subscription/sub_count' \
		--compressed \
		-X POST \
		-H 'User-Agent: Mozilla/5.0 (Windows NT 10.0; rv:114.0) Gecko/20100101 Firefox/114.0' \
		-H 'Accept: */*' \
		-H 'Accept-Language: en-US,en;q=0.5' \
		-H 'Accept-Encoding: gzip, deflate, br' \
		-H 'Referer: https://odysee.com/' \
		-H 'Content-Type: application/x-www-form-urlencoded' \
		-H 'Origin: https://odysee.com' \
		-H 'DNT: 1' \
		-H 'Connection: keep-alive' \
		-H 'Sec-Fetch-Dest: empty' \
		-H 'Sec-Fetch-Mode: cors' \
		-H 'Sec-Fetch-Site: same-site' \
		-H 'TE: trailers' \
		--data-raw 'auth_token=CSZk7LQbxBSfNPCyzBmVNYxTuH4FBkrN&claim_id='${channel_claim_id}'' )"

	sub_count="$( jq '.data[0]' <<< "$json" )"
	echo $sub_count
}

[ -n "$channel_claim_id" ] && get_by_channel
