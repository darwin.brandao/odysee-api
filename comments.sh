#!/bin/bash

# Store the script path in a variable
DIR=$( cd -- "$( dirname "$(readlink -f "$0")" )" &> /dev/null && pwd )

num_comments=3

function usage()
{
    echo "Usage: ${0} [OPTIONS] [-b VALUE]"
    echo "  -v      Video Claim Id"
    echo "  -l      Sort by latest"
    echo "  -n      Number of comments"
    echo "  -h      Display help"
    exit 1
}

# Input Handler
source "${DIR}/input-handlers/comments.src"

function get_by_video() {
	json="$( curl -s 'https://comments.odysee.tv/api/v2?m=comment.List' \
		-X POST \
		-H 'User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:109.0) Gecko/20100101 Firefox/114.0' \
		-H 'Accept: */*' \
		-H 'Accept-Language: en-US,pt-BR;q=0.7,en;q=0.3' \
		-H 'Accept-Encoding: gzip, deflate, br' \
		-H 'Content-Type: application/json' \
		-H 'Referer: https://odysee.com/' \
		-H 'Origin: https://odysee.com' \
		-H 'Sec-Fetch-Dest: empty' \
		-H 'Sec-Fetch-Mode: cors' \
		-H 'Sec-Fetch-Site: cross-site' \
		-H 'Connection: keep-alive' \
		-H 'TE: trailers' \
		--data-raw '
			{
				"jsonrpc":"2.0",
				"id":1,
				"method":"comment.List",
				"params":
				{
					"page":1,
					"claim_id":"'${video_claim}'",
					"page_size":10,
					"top_level":true,
					"channel_id":"30576d9991a7f39d619b4077807b8ed338cc4a1c",
					"channel_name":"@CypherpunksToday",
					"sort_by":3
				}
			}
		' | jq . )"

		echo "$json"
}

function get_latest {
	json="$( get_by_video | jq ".result.items | sort_by(.timestamp) | reverse[:${num_comments}]" )"
	echo "$json"
}

output="$( [ "$latest" == true ] && get_latest || get_by_video )"

[ "$count" == "true" ] && jq '.result.items | length' <<< "$output" || echo "$output"
