#!/bin/bash

# Store the script path in a variable
DIR=$( cd -- "$( dirname "$(readlink -f "$0")" )" &> /dev/null && pwd )

function usage()
{
    echo "Usage: ${0} [OPTIONS] [-b VALUE]"
    echo "  -i      Channel Id"
    echo "  -b      Argument b"
    echo "  -h      Display help"
    exit 1
}

# Input Handler
source "${DIR}/input-handlers/videos.src"

function get_by_channel() {
	json="$( curl -s 'https://api.na-backend.odysee.com/api/v1/proxy?m=claim_search' \
		--compressed \
		-X POST \
		-H 'User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:109.0) Gecko/20100101 Firefox/114.0' \
		-H 'Accept: */*' \
		-H 'Accept-Language: en-US,pt-BR;q=0.7,en;q=0.3' \
		-H 'Accept-Encoding: gzip, deflate' \
		-H 'Content-Type: application/json-rpc' \
		-H 'Referer: https://odysee.com/' \
		-H 'Origin: https://odysee.com' \
		-H 'Sec-Fetch-Dest: empty' \
		-H 'Sec-Fetch-Mode: cors' \
		-H 'Sec-Fetch-Site: same-site' \
		-H 'Connection: keep-alive' \
		-H 'TE: trailers' \
		--data-raw '
			{
				"jsonrpc":"2.0",
				"method":"claim_search",
				"params":
				{
					"page_size":100,
					"page":1,
					"channel_ids":["'${channel_id}'"],
					"stream_types":["video","audio","document","image","model","binary"],
					"claim_type":"stream",
					"order_by":["release_time"],
					"no_totals":true,
					"index":1,
					"has_source":true
				},
				"id":1687509063468
			}'
		)"
	echo "$json"
}

function get_count() {
	json="$( get_by_channel | jq ".result.items | length" )"
	echo "$json"
}

[ "$count" == "true" ] && get_count || get_by_channel
