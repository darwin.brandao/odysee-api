# Odysee API

## Comments on given video (change VIDEO_CLAIM_ID)

```
curl -s 'https://comments.odysee.tv/api/v2?m=comment.List' -X POST -H 'User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:109.0) Gecko/20100101 Firefox/114.0' -H 'Accept: */*' -H 'Accept-Language: en-US,pt-BR;q=0.7,en;q=0.3' -H 'Accept-Encoding: gzip, deflate, br' -H 'Content-Type: application/json' -H 'Referer: https://odysee.com/' -H 'Origin: https://odysee.com' -H 'Sec-Fetch-Dest: empty' -H 'Sec-Fetch-Mode: cors' -H 'Sec-Fetch-Site: cross-site' -H 'Connection: keep-alive' -H 'TE: trailers' --data-raw '{"jsonrpc":"2.0","id":1,"method":"comment.List","params":{"page":1,"claim_id":"{VIDEO_CLAIM_ID}","page_size":10,"top_level":true,"channel_id":"30576d9991a7f39d619b4077807b8ed338cc4a1c","channel_name":"@CypherpunksToday","sort_by":3}}' | jq .
```

## View count on given video

```
curl 'https://api.odysee.com/file/view_count' --compressed -X POST -H 'User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:109.0) Gecko/20100101 Firefox/114.0' -H 'Accept: */*' -H 'Accept-Language: en-US,pt-BR;q=0.7,en;q=0.3' -H 'Accept-Encoding: gzip, deflate, br' -H 'Content-Type: application/x-www-form-urlencoded' -H 'Referer: https://odysee.com/' -H 'Origin: https://odysee.com' -H 'Sec-Fetch-Dest: empty' -H 'Sec-Fetch-Mode: cors' -H 'Sec-Fetch-Site: same-site' -H 'Connection: keep-alive' -H 'TE: trailers' --data-raw 'auth_token=AAqxgB1kjZGGE4qHvhw9y8DorRrkygce&claim_id={VIDEO_CLAIM_ID}'
```

## Subscribers count (channel id)

```
curl 'https://api.odysee.com/subscription/sub_count' --compressed -X POST -H 'User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:109.0) Gecko/20100101 Firefox/114.0' -H 'Accept: */*' -H 'Accept-Language: en-US,pt-BR;q=0.7,en;q=0.3' -H 'Accept-Encoding: gzip, deflate, br' -H 'Content-Type: application/x-www-form-urlencoded' -H 'Referer: https://odysee.com/' -H 'Origin: https://odysee.com' -H 'Sec-Fetch-Dest: empty' -H 'Sec-Fetch-Mode: cors' -H 'Sec-Fetch-Site: same-site' -H 'Connection: keep-alive' -H 'TE: trailers' --data-raw 'auth_token=AAqxgB1kjZGGE4qHvhw9y8DorRrkygce&claim_id={CHANNEL_CLAIM_ID}'
```

## Latest 10 videos (channel id)
```
curl 'https://api.na-backend.odysee.com/api/v1/proxy?m=claim_search' --compressed -X POST -H 'User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:109.0) Gecko/20100101 Firefox/114.0' -H 'Accept: */*' -H 'Accept-Language: en-US,pt-BR;q=0.7,en;q=0.3' -H 'Accept-Encoding: gzip, deflate, br' -H 'Content-Type: application/json-rpc' -H 'Referer: https://odysee.com/' -H 'Origin: https://odysee.com' -H 'Sec-Fetch-Dest: empty' -H 'Sec-Fetch-Mode: cors' -H 'Sec-Fetch-Site: same-site' -H 'Connection: keep-alive' -H 'TE: trailers' --data-raw '{"jsonrpc":"2.0","method":"claim_search","params":{"page_size":11,"page":1,"channel_ids":["{CHANNEL_ID}"],"stream_types":["video","audio","document","image","model","binary"],"claim_type":"stream","order_by":["release_time"],"no_totals":true,"index":1,"has_source":true},"id":1687492973923}'
```

# Check if it's live (channel claim id)
```
curl 'https://api.odysee.live/livestream/is_live' --compressed -X POST -H 'User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:109.0) Gecko/20100101 Firefox/114.0' -H 'Accept: */*' -H 'Accept-Language: en-US,pt-BR;q=0.7,en;q=0.3' -H 'Accept-Encoding: gzip, deflate, br' -H 'Content-Type: application/x-www-form-urlencoded' -H 'Referer: https://odysee.com/' -H 'Origin: https://odysee.com' -H 'Sec-Fetch-Dest: empty' -H 'Sec-Fetch-Mode: cors' -H 'Sec-Fetch-Site: cross-site' -H 'Connection: keep-alive' -H 'TE: trailers' --data-raw 'channel_claim_id={CHANNEL_CLAIM_ID}'
```

# Subs count (channel claim id)
```
curl -s 'https://api.odysee.com/subscription/sub_count' --compressed -X POST -H 'User-Agent: Mozilla/5.0 (Windows NT 10.0; rv:114.0) Gecko/20100101 Firefox/114.0' -H 'Accept: */*' -H 'Accept-Language: en-US,en;q=0.5' -H 'Accept-Encoding: gzip, deflate, br' -H 'Referer: https://odysee.com/' -H 'Content-Type: application/x-www-form-urlencoded' -H 'Origin: https://odysee.com' -H 'DNT: 1' -H 'Connection: keep-alive' -H 'Sec-Fetch-Dest: empty' -H 'Sec-Fetch-Mode: cors' -H 'Sec-Fetch-Site: same-site' -H 'TE: trailers' --data-raw 'auth_token=CSZk7LQbxBSfNPCyzBmVNYxTuH4FBkrN&claim_id={CHANNEL_CLAIM_ID}' | jq '.data[0]'
```
