#!/bin/bash

# Store the script path in a variable
DIR=$( cd -- "$( dirname "$(readlink -f "$0")" )" &> /dev/null && pwd )

function usage()
{
    echo "Usage: ${0} [OPTIONS] [-b VALUE]"
    echo "  -a      Option a"
    echo "  -b      Argument b"
    echo "  -h      Display help"
    exit 1
}

# Input Handler
source "${DIR}/input-handlers/view-count.src"

function get_by_video() {
	json="$( 
		curl -s 'https://api.odysee.com/reaction/list' \
		--compressed \
		-X POST \
		-H 'User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:109.0) Gecko/20100101 Firefox/114.0' \
		-H 'Accept: */*' \
		-H 'Accept-Language: en-US,pt-BR;q=0.7,en;q=0.3' \
		-H 'Accept-Encoding: gzip, deflate, br' \
		-H 'Content-Type: application/x-www-form-urlencoded' \
		-H 'Referer: https://odysee.com/' \
		-H 'Origin: https://odysee.com' \
		-H 'Sec-Fetch-Dest: empty' \
		-H 'Sec-Fetch-Mode: cors' \
		-H 'Sec-Fetch-Site: same-site' \
		-H 'Connection: keep-alive' \
		-H 'TE: trailers' \
		--data-raw 'auth_token=AAqxgB1kjZGGE4qHvhw9y8DorRrkygce&claim_ids='${video_claim_id}''
	)"
	jq '.data.others_reactions.'${video_claim_id}'.like' <<< "$json"
}

get_by_video
